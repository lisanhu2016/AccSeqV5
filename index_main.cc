#include "accIndex.h"
#include "logger.h"


static void usage(const char *program);


void usage(const char *program) {
    Logger::Error(program, "{REFERENCE_FILE}");
    Logger::Error("\t For example: ", program, "reference.fa");
}

int main(int argc, const char *argv[]) {
    if (argc != 2) {
        usage(argv[0]);
        return -1;
    }

    index(argv[1]);
    return 0;
}