//
// Created by Sanhu Li on 10/05/17.
//
#include <cstdint>
#include <zlib.h>
#include "accIndex.h"
#include "logger.h"
#include "sais/sais.h"
#include "RRR/WaveletTree.h"
#include "kseq.h"

KSEQ_INIT(gzFile, gzread)


typedef std::pair<int, int> range;

using std::cout;
using std::endl;

static range
fm_index(const char *query, int n, const DataStructure::WaveletTree &wt, const int *count,
         const uint8_t mapper[256]);

static void num_to_seq(char *buf, int num);

static std::pair<char *, long> load_file(const char *filename);

static void
compute_hash_table(int *hash, const char *B, size_t len, const int *C, const uint8_t *mapper);

static void
compute_hash_table_dbg(int *hash, const char *B, size_t len, const int *C, const uint8_t *mapper,
                       const char *db, int32_t *sa);

static void count(const uint8_t *text, int n, const uint8_t mapper[256], int32_t *c);

static void pack_fa_file(const char *file_name);

std::pair<char *, long> load_file(const char *filename) {
    FILE *fp = fopen(filename, "rb");
    long sz = file_size(fp);
    auto buf = new char[sz + 1];
    fread(buf, sizeof(char), static_cast<size_t>(sz), fp);

    for (int i = 0; i < sz; ++i) {
        switch (buf[i]) {
            case 'a':
            case 'c':
            case 'g':
            case 't':
                buf[i] -= 32;
            case 'A':
            case 'C':
            case 'G':
            case 'T':
                break;
            default:
                buf[i] = 'Z';
        }
    }

    buf[sz] = 0;
    fclose(fp);
    return {buf, sz};
}

void index(const char *file_name) {
    uint8_t mapper[256] = {};
    mapper[0] = 0;
    mapper['A'] = mapper['a'] = 1;
    mapper['C'] = mapper['c'] = 2;
    mapper['G'] = mapper['g'] = 3;
    mapper['T'] = mapper['t'] = 4;
    mapper['Z'] = 5;

    Logger::Verbose("Done initializing, begin packing", file_name);
    double b = get_prog_time();

    pack_fa_file(file_name);
    Logger::Verbose("Done packing", file_name, "for", FTOA(get_prog_time() - b), "seconds");

    Logger::Verbose("Done packing, begin loading...");
    b = get_prog_time();
    std::string fn = file_name;
    auto contents = load_file((fn + ".pac").c_str());
    Logger::Verbose("Done loading for", FTOA(get_prog_time() - b), "seconds");
    Logger::Verbose("Begin allocating space for computation");

    auto sa = new int32_t[contents.second + 1];
    auto B = new uint8_t[contents.second + 1];
    auto T = (uint8_t *) contents.first;

    Logger::Verbose("Done memory allocation.");
    Logger::Verbose("Begin computing suffix array...");
    b = get_prog_time();

    sais(T, sa, static_cast<int>(contents.second + 1));

    Logger::Verbose("Done computing suffix array for", FTOA(get_prog_time() - b), "seconds");
    Logger::Verbose("Begin computing BWT...");
    b = get_prog_time();

    sa_bwt(T, sa, static_cast<int>(contents.second + 1), B);

    Logger::Verbose("Done computing BWT for", FTOA(get_prog_time() - b), "seconds");

    std::string fname = file_name;

    auto sa_name = fname + ".sa";
    Logger::Verbose("Begin writing", sa_name.c_str());
    b = get_prog_time();

    write_file(sa, int32_t, static_cast<size_t>(contents.second + 1), sa_name.c_str())

    Logger::Verbose("Done writing SA file using", FTOA(get_prog_time() - b), "seconds");

    int32_t c[6]{};
    auto hash = new int32_t[1 << 25];
    count((const uint8_t *) (contents.first), static_cast<int>(contents.second + 1), mapper, c);
    b = get_prog_time();
    Logger::Verbose("Begin computing hash table");
//    compute_hash_table(hash, (const char *) B, (size_t) contents.second + 1, c, mapper);
    compute_hash_table_dbg(hash, (const char *) B, (size_t) contents.second + 1, c, mapper,
                           reinterpret_cast<const char *>(T), sa);
    Logger::Verbose("Done computing hash table using", FTOA(get_prog_time() - b), "seconds");

    auto hash_name = fname + ".hash";
    auto cnt_name = fname + ".cnt";
    Logger::Verbose("Begin writing", hash_name.c_str(), "and", cnt_name.c_str());
    b = get_prog_time();

    write_file(c, int32_t, 6, cnt_name.c_str())
    write_file(hash, int32_t, 1 << 25, hash_name.c_str())
    Logger::Verbose("Done writing hash file using", FTOA(get_prog_time() - b), "seconds");
}

void compute_hash_table(int *hash, const char *B, size_t len, const int *C, const uint8_t *mapper) {
    DataStructure::WaveletTree wt;
    wt.init();

    for (size_t i = 0; i < len; ++i) {
        char cc = B[i];
        wt.push(cc);
    }

    char buf[13] = {};
    for (int i = 0; i < (1 << 24); ++i) {
        num_to_seq(buf, i);
        range r = fm_index(buf, 12, wt, C, mapper);
        hash[2 * i] = r.first;
        hash[2 * i + 1] = r.second;
    }
}

static void count(const uint8_t *text, int n, const uint8_t mapper[256], int32_t *c) {
    for (int i = 0; i < n; ++i) {
        char cc = text[i];
//        for (int j = mapper[cc] + 1; j < 6; ++j) {
//            c[j] += 1;
//        }
        switch (cc) {
            case '\0':
                c[1]++;
            case 'A':
                c[2]++;
            case 'C':
                c[3]++;
            case 'G':
                c[4]++;
            case 'T':
                c[5]++;
            default:
            case 'Z':
                ;
        }
    }
//    return;
}

static range
fm_index(const char *query, int n, const DataStructure::WaveletTree &wt, const int *count,
         const uint8_t mapper[256]) {
    int i = n - 1;
    char c = query[i--];
    range r;
    r.first = count[mapper[c]];
    r.second = count[mapper[c] + 1] - 1;
    for (; i >= 0; --i) {
        c = query[i];
        r.first = int(count[mapper[c]] + wt.rank(c, r.first - 1));
        r.second = int(count[mapper[c]] + wt.rank(c, r.second) - 1);
        if (r.second < r.first) break;
    }
    return r;
}

void num_to_seq(char *buf, int num) {
    int mask = 0x3;
    auto map = "ACGT";
    for (int i = 0; i < 12; ++i) {
        buf[11 - i] = map[(num >> (2 * i)) & mask];
    }
}

void
compute_hash_table_dbg(int *hash, const char *B, size_t len, const int *C, const uint8_t *mapper,
                       const char *db, int32_t *sa) {
    DataStructure::WaveletTree wt;
    wt.init();
    const int MAX_RPT_TIME = 10000;
    bool show_msg = true;
    auto counts = new int[MAX_RPT_TIME]{};


    for (size_t i = 0; i < len; ++i) {
        char cc = B[i];
        wt.push(cc);
    }

    char buf[13] = {};
    for (int i = 0; i < (1 << 24); ++i) {
        num_to_seq(buf, i);
        range r = fm_index(buf, 12, wt, C, mapper);
        hash[2 * i] = r.first;
        hash[2 * i + 1] = r.second;

        auto fst = r.first;
        auto snd = r.second;
        int n = 0;
        if (snd >= fst) {
            n = snd - fst + 1;
        }
        if (n >= MAX_RPT_TIME) {
            using std::cerr;
            using std::endl;
            if (show_msg) {
                cerr << "Duplicates more than " << MAX_RPT_TIME << " found" << endl;
                show_msg = false;
            }
            continue;
        } else {
            counts[n] += 1;
        }

        for (int j = fst; j <= snd; ++j) {
            auto pos = sa[j];
            auto s = strndup(db + pos, 12);
            if (strcmp(s, buf) != 0) {
                std::cout << s << std::endl;
                std::cout << buf << std::endl;
            }
        }
    }


    using std::cout;
    for (int i = 0; i < MAX_RPT_TIME; ++i) {
        cout << i << ", " << counts[i] << endl;
    }

}

static void process_seq_name(char *seq_name) {
    size_t len = strlen(seq_name);
    for (int i = 0; i < len; ++i) {
        if (seq_name[i] == ' ' || seq_name[i] == '\t' || seq_name[i] == '\n' ||
            seq_name[i] == '\r') {
            seq_name[i] = 0;
            break;
        }
    }
}

void pack_fa_file(const char *file_name) {
//
//    using std::cout;
//    using std::endl;
//
    gzFile fp;
    kseq_t *seq;
    fp = gzopen(file_name, "r");
    std::string fn = file_name;

    /// ref.fa.pac is packed gene sequence file
    auto opfp = fopen((fn + ".pac").c_str(), "w");
    /// ref.fa.chn contains ref chromosome names divided by new line
    /// ref.fa.fai contains the end position of the chromosome (0-index)
    /// including the divider
    auto ocfp = fopen((fn + ".chn").c_str(), "w");
    auto oifp = fopen((fn + ".fai").c_str(), "w");
    /// use 100 'Z' to divide different genes in pac file
    std::string divider(DIV_LENGTH, 'Z');

    seq = kseq_init(fp);
    size_t pos = 0;
    while (kseq_read(seq) >= 0) {
        auto rname = seq->name.s;
        process_seq_name(rname);
        fprintf(ocfp, "%s\n", rname);
        fprintf(opfp, "%s%s", seq->seq.s, divider.c_str());
        pos += seq->seq.l + 100;
        fwrite(&pos, sizeof(size_t), 1, oifp);
//        cout << rname << " " << ftell(opfp) << endl;
    }
    kseq_destroy(seq);
    gzclose(fp);
    fclose(opfp);
    fclose(ocfp);
    fclose(oifp);
}

int main(int argc, const char *argv[]) {
    if (argc == 2) {
        index(argv[1]);
    } else {
        using std::cerr;
        using std::endl;
        cerr << "Wrong input parameters" << endl;
    }
    return 0;
}
