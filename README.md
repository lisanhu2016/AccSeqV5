# AccSeq: A High-Level Parallel NGS tool using Novel Time-Memory Efficient Algorithm

__By: Sanhu Li, Sunita Chandrasekaran__

## Introduction

AccSeq is a high-level infrastructure that could help build novel NGS alignment algorithms. 
It models the NGS alignment problem into three steps:
1. From original reads, map to another set of reads, further referred as reads'.
2. Apply exact string matching algorithm to reads'.
3. Reassemble the information for the reads from the position information of reads'.

With AccSeq, scientists could easily create new NGS alignment algorithm by 
using best algorithms for each step. 
For example, BWA could be treated as using BWA heuristic function to 
reduce a brute-force search space in step 1; using FM-index algorithm for exact string matching
task in step 2; using information from previous steps and Smith-Waterman algorithm in step 3. 
By changing the step 3 Smith-Waterman algorithm to some other algorithm (some edit distance algorithm),
we could easily create a different NGS alignment algorithm with this simple step.

In this repo, we have created our own tool, which is also called AccSeq, 
that uses this infrastructure, uses novel algorithms in each step, and is paralleled using
high-level, portable HPC framework OpenACC. Our tool could get results in much shorter time than BWA,
and some other the-state-of-the-art tools like BarraCUDA which maintaining great accuracy and sensitivity.

## Usage

### Infrastructure Usage

If you would like to create your own NGS alignment algorithm, you only need to keep the following files:

* accIndex.h
* accSeq.h
* utils.h utils.cc
* logger.h
* index_main.cc

You must define the following classes/functions/files to create your own algorithm:

`accIndex.h`
```C
    void index(const char *file_name);
```

`index_main.cc`
```
    static void usage(const char *program);
```

All structures and functions in accSeq.h

Create your own `aln_main.cc` for alignment main function. You could refer to our file `aln_main.cc`.

### Tool Usage

#### Compiling

For default serial code compilation:
```
$ cd ${PATH_TO_THE_FOLDER}
$ mkdir build && cd build
$ cmake ../ -DCMAKE_BUILD_TYPE=Release
$ make 
```

For PGI compiler in serial:
```
$ cd ${PATH_TO_THE_FOLDER}
$ export CC=pgcc; export CXX=pgc++;
$ mkdir build && cd build
$ cmake ../ -DCMAKE_BUILD_TYPE=Release
$ make 
```

For PGI compiler in Multi-core platform:
```
$ cd ${PATH_TO_THE_FOLDER}
$ export CC=pgcc; export CXX=pgc++;
$ mkdir build && cd build
$ cmake ../ -DCMAKE_BUILD_TYPE=Release -DMC=on
$ make 
```

For PGI compiler in NVIDIA platform:
```
$ cd ${PATH_TO_THE_FOLDER}
$ export CC=pgcc; export CXX=pgc++;
$ mkdir build && cd build
$ cmake ../ -DCMAKE_BUILD_TYPE=Release -DNVIDIA=On
$ make 
```

If your device compute capability is not 60, you should change the `cc60` in the `CMakeLists.txt` to what you should use.

#### Running

* First, we need to build index for the genome

```
$ ./accIndex ${FA_FILE}
```

* Then, we need to do alignment and generate SAM file

```
$ # For single end reads
$ ./accSeq ${FA_FILE} ${FQ_FILE} ${OUTPUT_PATH}
```

```
$ # For pair end reads
$ ./accSeq ${FA_FILE} ${FQ_FILE1} ${FQ_FILE2} ${OUTPUT_PATH}
```

### Other information

If you have any questions, feel free to contact [Sanhu Li](mailto:lisanhu@udel.edu) and 
[Dr. Sunita Chandrasekaran](mailto:schandra@udel.edu)

The project is still under active development and feel free to email the authors when you have any problem using it.
