//
// Created by Sanhu Li on 10/06/17.
//
#include <cstdio>
#include <string>
#include <zlib.h>
#include <map>
#include <iostream>
#include <vector>

#include "accSeq.h"
#include "utils.h"
#include "kseq.h"
#include "edlib/edlib.h"
#include "logger.h"

#include "config.h"

KSEQ_INIT(gzFile, gzread)


//typedef std::pair<const char *, int> String;

//typedef std::pair<uint32_t, uint16_t> HistoBar;
typedef struct HistoBar {
    uint32_t pos;
    uint16_t count;
//    std::vector<size_t> rids;
//    std::vector<uint32_t> voted_pos;
} HistoBar;
typedef std::vector<HistoBar> Histogram;

static int seq_to_num(const char *seq);

static std::pair<char *, long> load_file(const char *filename);

static void load_queries(const char *filename, std::vector<Query> &queries);

//static void count(const uint8_t *text, int n, const uint8_t mapper[256], int32_t *c);

static size_t *generate_fai_pos_hash(std::vector<fai> &fais, size_t file_size, const int offset);


void load_index(const char *file_name, context *global_context) {
    std::string fname = file_name;
    auto pac_fn = fname + ".pac";
    auto db = fopen(pac_fn.c_str(), "r");
    long fsz = file_size(db);
    fclose(db);
    auto sa = new int32_t[fsz + 1];
    auto hash = new int32_t[1 << 25];
    auto db_txt = new char[fsz + 1];

#define CSTR(x) ((x).c_str())
    auto hash_fname = strdup(CSTR(fname + ".hash"));
    auto sa_fname = strdup(CSTR(fname + ".sa"));
    read_file(sa, int32_t, size_t(fsz + 1), sa_fname)
    read_file(hash, int32_t, 1 << 25, hash_fname)
    read_file(db_txt, char, size_t(fsz + 1), pac_fn.c_str())

    global_context->hash = hash;
    global_context->sa = sa;
    global_context->db = db_txt;
    global_context->dbsize = static_cast<size_t>(fsz);


    /// load chromosome name and offsets information
    std::vector<const char *> lines;
    read_lines(CSTR(fname + ".chn"), lines);
    auto pfp = fopen(CSTR(fname + ".fai"), "r");
    size_t fai_pos;
    auto & fais = global_context->fais;
    for (size_t i = 0; i < lines.size(); ++i) {
        fread(&fai_pos, sizeof(size_t), 1, pfp);
        fai f{lines[i], fai_pos};
        fais.push_back(f);
    }
    fclose(pfp);

    global_context->fai_hash = generate_fai_pos_hash(fais, static_cast<size_t>(fsz + 1), global_context->fai_pos_offset);

//    for (auto fai : global_context->fais) {
//        std::cout << fai.first << " " << fai.second << std::endl;
//    }

#undef CSTR
}

void read_queries(std::vector<Query> &queries, const char *file_name) {
    load_queries(file_name, queries);

    for (size_t i = 0; i < queries.size(); ++i) {
        queries[i].qid = i;
    }
}

void num_to_seq(char *buf, int num) {
    int mask = 0x3;
    auto map = "ACGT";
    for (int i = 0; i < 12; ++i) {
        buf[11 - i] = map[(num >> (2 * i)) & mask];
    }
    buf[12] = 0;
}

//void alignment(std::vector<Align> &result, std::vector<Query> &reads, context *global_context) {
//    auto num_reads = reads.size();
//    auto hash = global_context->hash;
//
//    auto *res = new Align[num_reads];
//
////#pragma acc parallel loop
//    for (size_t i = 0; i < num_reads; ++i) {
////        Align r{};
//        Align &r = res[i];
//        int num = seq_to_num(reads[i].seq);
////        if (i == 686450) {
////            std::cout << num << std::endl;
////            std::cout << strndup(reads[i].seq, 12) << std::endl;
////            std::cout << std::hex << size_t(reads[0].seq) << std::dec << std::endl;
////            std::cout << std::hex << size_t(reads[1].seq) << std::dec << std::endl;
////            std::cout << std::hex << size_t(reads[i].seq) << std::dec << std::endl;
////        }
//        r.rid = i;
//        r.qid = reads[i].qid;
//        r.first = hash[num * 2];
//        r.second = hash[num * 2 + 1];
//        r.off = reads[i].off;
////        result.push_back(r);
//    }
//
//    result.insert(result.end(), res, res + num_reads);
//    delete[] res;
//}

void alignment(std::vector<Align> &result, std::vector<Query> &reads, context *global_context) {
    auto num_reads = reads.size();
    auto hash = global_context->hash;
    auto seqs = global_context->reads_seq;
    auto seq_len = global_context->reads_seq_length;
    auto qlen = global_context->read_length;

    auto rs = reads.data();
    const size_t group_size = 1000000;
    auto res = new Align[group_size];

#pragma acc enter data copyin(hash[:1<<25], seqs[:seq_len], rs[:num_reads])
    for (size_t j = 0; j < num_reads; j += group_size) {
#pragma acc parallel loop present(hash, seqs, rs) copyout(res[:group_size])
        for (size_t i = 0; i < group_size; ++i) {
            if (i + j < num_reads) {
                auto qid = rs[(i + j)].qid;
                auto off = rs[(i + j)].off;
                int num = seq_to_num(seqs + qid * qlen + off);
//                if (i + j == 686450) {
//                    std::cout << num << std::endl;
//                    std::cout << std::hex << size_t(seqs) << std::dec << std::endl;
//                    std::cout << std::hex << size_t(seqs + 1 * READ_DIST) << std::dec << std::endl;
//                    std::cout << strndup(seqs + qid * qlen + off, 12) << std::endl;
//                    std::cout << std::hex << size_t(seqs + qid * qlen + off) << std::dec << std::endl;
//                }
                res[i].rid = (i + j);
                res[i].qid = rs[(i + j)].qid;
                res[i].first = hash[num * 2];
                res[i].second = hash[num * 2 + 1];
                res[i].off = off;
            }
        }
        result.insert(result.end(), res, num_reads - j >= group_size ? res + group_size : res + num_reads - j);
    }
#pragma acc exit data finalize
    delete[] res;
}

void process_queries(std::vector<Query> &in, std::vector<Query> &out) {
    // in order for this to work, we have to assert length of all queries are the same
    if (in.empty()) {
        return;
    }
    auto reads_num = (in[0].len - READ_LENGTH) / READ_DIST + 1;
    auto num = in.size();
    auto sz = num * reads_num;
    auto o = new Query[sz];

    auto id = in.data();

#pragma acc parallel loop collapse(2) copyin(id[:num]) copyout(o[:sz])
    for (size_t i = 0; i < num; ++i) {
//        for (size_t j = 0; j <= q.len - READ_LENGTH; j += READ_DIST) {
        for (size_t j = 0; j < reads_num; ++j) {
            const Query &q = id[i];
//            Query qq(q.name, q.seq + j, READ_LENGTH, q.qual + j);
            Query &qq = o[i * reads_num + j];
//            Query qq(q.name, q.seq + j * READ_DIST, READ_LENGTH, q.qual + j * READ_DIST);
            qq.name = q.name;
            qq.seq = q.seq + j * READ_DIST;
            qq.len = READ_LENGTH;
            qq.qual = q.qual + j * READ_DIST;
            qq.qid = i;
            qq.off = j * READ_DIST;
//            Query qqq(q.name, q.seq + q.len - j - READ_LENGTH, READ_LENGTH, q.qual + q.len - j - READ_LENGTH);
//            qqq.qid = i;
//            qqq.off = q.len - j - READ_LENGTH;
//            out.push_back(qq);
//            out.push_back(qqq);
        }
    }

    out.insert(out.end(), o, o + sz);
    delete[] o;
}

//typedef std::pair<int, int> range;
//
//void range_minus(range a, range b, std::vector<range> &res) {
//    if (a.first < b.first and a.second > b.second) {
//        res.emplace_back(a.first, b.first - 1);
//        res.emplace_back(b.second + 1, a.second);
//    } else if (a.first > b.second) {
//        return;
//    } else if (a.first >= b.first and a.second)
//}

typedef struct edit_distance {
    int d;
    char *cigar;
} edit_distance;


edit_distance
compute_edit_distance(const char *s1, int s1_len, const char *s2, int s2_len, int d_limit) {
    static auto cfg = edlibNewAlignConfig(d_limit, EDLIB_MODE_NW, EDLIB_TASK_PATH, nullptr, 0);
    EdlibAlignResult result = edlibAlign(s1, s1_len, s2, s2_len, cfg);
    auto c = edlibAlignmentToCigar(result.alignment, result.alignmentLength,
                                   EDLIB_CIGAR_STANDARD);
    return {result.editDistance, c};
}


void parse_histogram(Histogram &histo, uint16_t thres, std::vector<Align> &alns, Query q, const char *db,
                     Result *r, context *ctx) {
    r->qid = q.qid;
    r->pos = -1;
    auto d_best = int(q.len);
    for (auto bar : histo) {
        if (bar.count >= thres && bar.pos < ctx->dbsize) {
            auto ed = compute_edit_distance(q.seq, int(q.len), db + bar.pos, int(q.len), d_best);
            if (ed.d < d_best and ed.d != -1) {
                d_best = ed.d;
                r->pos = bar.pos;
                r->CIGAR = ed.cigar;
                r->ed = d_best;
            }
        }
    }
}

void process_align(std::vector<Result> &result, std::vector<Align> &alignments,
                   context *global_context) {
    auto *histos = new Histogram[global_context->num_queries];

    auto errors = static_cast<const int>(global_context->read_length * 0.05);
    auto num_aln = alignments.size();
    auto bb = get_prog_time();

#ifdef MC
#pragma acc parallel loop
#endif
    for (size_t i = 0; i < num_aln; ++i) {
        auto aln = alignments[i];
        auto fst = aln.first;
        auto snd = aln.second;
//        if (i + 1 % 10000 == 0) {
//            std::cout << i << std::endl;
//        }
//        if (snd - fst + 1 > 400) {
//            continue;
//        }
#ifdef MC
#pragma acc loop seq
#endif
        for (auto j = fst; j <= snd; ++j) {
            bool voted = false;
            auto vote_pos = global_context->sa[j] - aln.off;

            for (auto &bar : histos[aln.qid]) {
                if (abs((int) bar.pos - (int) vote_pos) < errors) {
                    voted = true;
                    bar.count += 1;
                }
            }

            if (!voted and snd - fst + 1 <= 100) {
                HistoBar b;
                b.pos = static_cast<uint32_t >(vote_pos);
                b.count = 1;
                histos[aln.qid].push_back(b);
            }
        }
    }

    Logger::Verbose("Done voting in", FTOA(get_prog_time() - bb), "s");

    auto num_qs = global_context->num_queries;
    auto rs = new Result[num_qs];
#ifdef MC
#pragma acc parallel loop
#endif
    for (size_t i = 0; i < num_qs; ++i) {
        const Query &q = global_context->queries[i];
        auto reads_num = (q.len - READ_LENGTH) / READ_DIST + 1;
        auto thres = reads_num - q.len * ERROR_RATE * (READ_LENGTH / READ_DIST) - 1;
//        Result r{};
        auto &r = rs[i];
//        for (auto bar : histos[i]) {
//            if (bar.count >= thres)
//                std::cout << bar.pos << " : " << bar.count << std::endl;
//        }

	    parse_histogram(histos[i], static_cast<uint16_t>(thres), alignments, q, global_context->db,
	                    &r, global_context);
//        if (r.pos != -1) {
//            result.push_back(r);
//        }
    }
    result.insert(result.end(), rs, rs + num_qs);
    delete[] rs;
}

void process_seq_name(char *seq_name) {
    size_t len = strlen(seq_name);
    for (int i = 0; i < len; ++i) {
        if (seq_name[i] == ' ' || seq_name[i] == '\t' || seq_name[i] == '\n' ||
            seq_name[i] == '\r') {
            seq_name[i] = 0;
            break;
        }
    }
}

void write_result2(std::vector<Result> &results, context *global_context) {
    auto T = global_context->db;
    auto fd = fopen(global_context->out_path, "w");
    for (size_t i = 0; i < results.size(); ++i) {
        Result &r = results[i];
        int count = 0;
        int j, k;
        for (j = 0; j < r.pos + 1; ++j) {
            const char &c = T[r.pos - j];
            if (c != '\n' and c != '\r' and c != '\t' and c != ' ' and c != '>') {
                count++;
            } else if (c == '>') {
                break;
            }
        }

        for (k = 0; k < count; ++k)
            if (T[r.pos - j + k + 1] == '\n')
                break;

        auto rname = strndup(T + r.pos - j + 1, size_t(k));
        process_seq_name(rname);
        auto pos = count - k + 5;
//        std::cout << k << std::endl;
//        std::cout << pos << std::endl;

        auto queries = global_context->queries;
        auto query = queries[r.qid];
//        std::cout << query.name << std::endl;
//        std::cout << strndup(global_context->db+r.pos, 100) << std::endl;
        fprintf(fd, "%s\t%d\t%s\t%d\t%d\t%s\t%s\t%d\t%d\t%s\t%s\tED:i:%d\n", query.name, 0, rname, pos, 255,
                r.CIGAR, "*", 0, 0, query.seq, query.qual, r.ed);
    }
    fflush(fd);
    fclose(fd);
}


void write_result(std::vector<Result> &results, context *global_context) {
    auto fd = fopen(global_context->out_path, "w");
    char qbuf[global_context->read_length + 1];
    for (size_t i = 0; i < results.size(); ++i) {
        Result &r = results[i];

        auto pos = r.pos;

        if (pos == -1) continue;

        auto entry = pos / global_context->fai_pos_offset;
        auto chrid = global_context->fai_hash[entry];
        auto fais = global_context->fais;
        for (; chrid < fais.size(); ++chrid) {
            if (fais[chrid].second > pos) break;
        }
        auto rname = fais[chrid].first;


        if (fais[chrid].second - DIV_LENGTH < pos) {
            chrid++;
            pos = 1;
        } else if (chrid > 0) {
            pos -= fais[chrid - 1].second;
        }
        /// for 1-based SAM files
        pos++;
//        std::cout << k << std::endl;
//        std::cout << pos << std::endl;

        auto queries = global_context->queries;
        auto query = queries[r.qid];
        strncpy(qbuf, query.seq, query.len);
        qbuf[query.len] = '\0';
//        std::cout << query.name << std::endl;
//        std::cout << strndup(global_context->db+r.pos, 100) << std::endl;
        fprintf(fd, "%s\t%d\t%s\t%d\t%d\t%s\t%s\t%d\t%d\t%s\t%s\tED:i:%d\n", query.name, 0, rname, pos, 255,
                r.CIGAR, "*", 0, 0, qbuf, query.qual, r.ed);
    }
    fflush(fd);
    fclose(fd);
}

static int compute_code(char c) {
    switch (c) {
        case 'A':
            return 0;
        case 'C':
            return 1;
        case 'G':
            return 2;
        case 'T':
            return 3;
        default:
            return 0;
    }
}

int seq_to_num(const char *seq) {
    int num = 0;
//    int mapper[256];
//    mapper['A'] = 0;
//    mapper['C'] = 1;
//    mapper['G'] = 2;
//    mapper['T'] = 3;
    for (int i = 0; i < READ_LENGTH; ++i) {
        num += compute_code(seq[i]);
        num <<= 2;
    }
    num >>= 2;
    return num;
}

std::pair<char *, long> load_file(const char *filename) {
    FILE *fp = fopen(filename, "rb");
    long sz = file_size(fp);
    auto buf = new char[sz + 1];
    fread(buf, sizeof(char), static_cast<size_t>(sz), fp);
    buf[sz] = 0;
    fclose(fp);
    return {buf, sz};
}

void load_queries(const char *filename, std::vector<Query> &queries) {
    gzFile fp;
    kseq_t *seq;
    fp = gzopen(filename, "r");
    seq = kseq_init(fp);
    while (kseq_read(seq) >= 0) {
        Query q{};
        q.name = strdup(seq->name.s);
        q.seq = strdup(seq->seq.s);
        q.len = seq->seq.l;
        q.qual = strdup(seq->qual.s);
        for (int i = 0; i < q.len; ++i) {
            if (q.seq[i] == 'a') q.seq[i] = 'A';
            if (q.seq[i] == 'c') q.seq[i] = 'C';
            if (q.seq[i] == 'g') q.seq[i] = 'G';
            if (q.seq[i] == 't') q.seq[i] = 'T';
        }
        queries.push_back(q);
    }
    kseq_destroy(seq);
    gzclose(fp);
}

size_t *generate_fai_pos_hash(std::vector<fai> &fais, size_t file_size, const int offset) {
    auto hash = new size_t[file_size / offset];
    size_t i = 0, j = 0;
    for (; (i + 1) * offset < file_size; ++i) {
        while (i * offset >= fais[j++].second);
        hash[i] = --j;
//        for (; i * offset > fais[j].second; ++j) ;
    }
    return hash;
}

//void count(const uint8_t *text, int n, const uint8_t mapper[256], int32_t *c) {
//    for (int i = 0; i < n; ++i) {
//        char cc = text[i];
//        for (int j = mapper[cc]; j < 5; ++j) {
//            c[j + 1] += 1;
//        }
//    }
//}
