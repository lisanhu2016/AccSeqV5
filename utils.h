//
// Created by Sanhu Li on 07/31/17.
//

#ifndef ACCSEQ_UTILS_H
#define ACCSEQ_UTILS_H

#include <chrono>
#include <cstdio>
#include <vector>

#define FTOA(v) (std::to_string(v).c_str())

#define write_file(buf,type,length,filename) { \
    FILE *fp = fopen(filename, "w"); \
    fwrite(buf, sizeof(type), length, fp); \
    fflush(fp); \
    fclose(fp); \
}

#define read_file(buf,type,length,filename) { \
    FILE *fp = fopen(filename, "r"); \
    fread(buf, sizeof(type), length, fp); \
    fclose(fp); \
}

#define READ_LENGTH 12
#define READ_DIST 3
#define ERROR_RATE (0.02)
#define DIV_LENGTH 100

extern std::chrono::time_point<std::chrono::system_clock> begin;

long file_size(FILE *stream);

double get_prog_time(std::chrono::time_point<std::chrono::system_clock> tp = begin);

void read_lines(const char *path, std::vector<const char *> &lines);

#endif //ACCSEQ_UTILS_H
