//
// Created by Sanhu Li on 10/06/17.
//

#ifndef ACCSEQ_ACCSEQ_H
#define ACCSEQ_ACCSEQ_H

#include <vector>
#include <cstdint>
#include <string>

typedef struct Align {
    uint64_t rid, qid;
    int first, second;
    size_t off;
} Align;

typedef struct Query {
    size_t qid, off;
    const char *name;
    char *seq;
    size_t len;
    const char *qual;

//    Query(const char *name, char *seq, size_t len, const char *qual) {
//        this->qid = 0;
//        this->off = 0;
//        this->name = name;
//        this->seq = seq;
//        this->len = len;
//        this->qual = qual;
//    }
} Query;

typedef std::pair<const char *, size_t> fai;

typedef struct {
    const int32_t *hash;
    const int32_t *sa;
    size_t num_queries;
    const Query *queries;
    const Query *reads;
    const char *db;
    const char *out_path;
    size_t reads_seq_length;
    const char *reads_seq;
    size_t read_length, dbsize;
    /// fais and fai_hash are used to store information to fast generate SAM files
    std::vector<fai> fais;
    size_t *fai_hash;
    const int fai_pos_offset = 1024;
} context;

typedef struct {
    size_t qid;
    int32_t pos, ed;
    char *CIGAR;
} Result;


void load_index(const char *file_name, context *global_context);

void read_queries(std::vector<Query> &queries, const char *file_name);

void process_queries(std::vector<Query> &in, std::vector<Query> &out);

void alignment(std::vector<Align> &result, std::vector<Query> &reads, context *global_context);
//void alignment2(std::vector<Align> &result, std::vector<Query> &reads, context *global_context);

void process_align(std::vector<Result> &result, std::vector<Align> &alignments,
                   context *global_context);

void write_result(std::vector<Result> &results, context *global_context);

#endif //ACCSEQ_ACCSEQ_H
