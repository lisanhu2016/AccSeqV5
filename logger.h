//
// Created by Sanhu Li on 07/27/17.
//

#ifndef ACCSEQ_LOGGER_H
#define ACCSEQ_LOGGER_H


#include <iostream>
#include <cstdio>
#include "utils.h"

extern std::chrono::time_point<std::chrono::system_clock> begin;

namespace Logger {
    static void output_concat(FILE *stream, char deli, const char *content) {
        fprintf(stream, content);
    }

    template <typename ... Ts>
    static void output_concat(FILE *stream, char deli, const char *content, Ts... contents) {
        fprintf(stream, content);
        fprintf(stream, "%c", deli);
        output_concat(stream, deli, contents...);
    }

    template <typename ... Ts>
    static void format_output(const char *level, FILE *stream, Ts ... contents) {
        auto now = std::chrono::system_clock::now();
        std::chrono::duration<double> passed = now - begin;
        const char *tpl = "[ %-7s %8.2lf ] ";
        fprintf(stream, tpl, level, passed.count());
        output_concat(stream, ' ', contents...);
        fprintf(stream, "\n");
    }

    template <typename ... Ts>
    static void Verbose(Ts... contents) {
        format_output("Verbose", stdout, contents...);
    }

    template <typename ... Ts>
    static void Info(Ts... contents) {
        format_output("Inform", stdout, contents...);
    }

    template <typename ... Ts>
    static void Warn(Ts... contents) {
        format_output("Warning", stderr, contents...);
    }

    template <typename ... Ts>
    static void Error(Ts... contents) {
        format_output("Error", stderr, contents...);
    }
}


#endif //ACCSEQ_LOGGER_H
