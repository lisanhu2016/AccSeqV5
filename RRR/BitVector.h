//
// Created by Sanhu Li on 05/22/17.
//

#ifndef RRR_BITVECTOR_H
#define RRR_BITVECTOR_H

#include <cstdint>

namespace DataStructure {
    typedef long size_mt;
    typedef uint8_t store_type_mt;

    typedef struct BitVector {
    private:
        /// Use default page size for most CPU architectures to get possible better performance
        /// Some explain \@ https://stackoverflow.com/a/24490367/6670993
        static const int INIT_SIZE = 1 << 12;
        static const int BITS_PER_BYTE = 8;

        /**
         * Class for BitVector
         * lower bits are prior to higher bits in terms of the original order
         *
         * capacity stores the number of store_type elements, not the number of bits
         * size stores the number of bits that are valid
         *
         * This setting is for convenience
         */
        store_type_mt *dat;
        size_mt capacity, size_m;
    public:
        size_mt size() {
            return size_m;
        }

        const store_type_mt *const data() const {
            return dat;
        }

        /**
         * Get the bit value at position i
         *
         * @note This function might be running on heterogeneous architectures, so better to remove
         * conditional jumps and too much function calls
         * It's also implemented within the header file to get "inline" advantages
         *
         * @param i the bit position to access
         * @return the bit value at position i
         */
        store_type_mt get(size_mt i) const {
            /// assume i < size for faster operations
            //    if (i < size) {
            size_mt bid = i / (BITS_PER_BYTE * sizeof(store_type_mt));
            size_mt off = i % (BITS_PER_BYTE * sizeof(store_type_mt));
            return dat[bid] >> off & 1;
            //    }
        }

        /**
         * Set the bit value val at position i
         *
         * @note This function might be running on heterogeneous architectures, so better to remove
         * conditional jumps and too much function calls
         * It's also implemented within the header file to get "inline" advantages
         *
         * @param i the bit position to access
         * @param val the bit value to be set
         */
        void set(size_mt i, store_type_mt val) {
            /// assume i < size for faster operations
            size_mt bid = i / (BITS_PER_BYTE * sizeof(store_type_mt));
            size_mt off = i % (BITS_PER_BYTE * sizeof(store_type_mt));
            dat[bid] ^= (-val ^ dat[bid]) & (1 << off);
        }

        void push(store_type_mt val) {
            /// This function gets inlined because it might be called many times
            if (size_m + 1 > capacity * (BITS_PER_BYTE * sizeof(store_type_mt))) {
                extend();
            }
            set(size_m++, val);
        }

        store_type_mt operator[](long pos) const {
            return pos < 0 ? -1 : get((size_mt) pos);
        }

        void destroy();
        void init();
        void init(size_mt cap);

    private:
        void extend();
    } bv_mt;
}

#endif //RRR_BITVECTOR_H
