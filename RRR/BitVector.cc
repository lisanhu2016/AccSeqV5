//
// Created by Sanhu Li on 05/22/17.
//

#include <cstdlib>
#include "BitVector.h"

void DataStructure::BitVector::init() {
    init(INIT_SIZE);
}

void DataStructure::BitVector::init(DataStructure::size_mt cap) {
    size_m = 0;
    capacity = cap;
    dat = (store_type_mt *) calloc(capacity, sizeof(store_type_mt));
}

void DataStructure::BitVector::extend() {
    capacity <<= 1;
    dat = (store_type_mt *) realloc(dat, capacity * sizeof(store_type_mt));
}

void DataStructure::BitVector::destroy() {
    capacity = 0;
    size_m = 0;
    free(dat);
}
