//
// Created by Sanhu Li on 05/22/17.
//

#ifndef RRR_RRR_H
#define RRR_RRR_H

#include "BitVector.h"
#include <vector>
#include <cstdint>

namespace DataStructure {
    extern uint8_t const rrr_table[256][8];
    using namespace std;

    class RRR {
        BitVector store;
        const int B = 8, F = 16;
        vector<size_mt> f_tabs;
        size_mt n = 0;

    public:
        size_mt size() {
            return store.size();
        }

        store_type_mt get(size_mt i) const {
            return store.get(i);
        }

        void push(store_type_mt val) {
            if ((store.size() % (F * B)) == 0) {
                /// if to get to a boundary for this push, store current # of 1s
                f_tabs.push_back(n);
            }
            if (val == 1) n++;
            store.push(val);
        }

        store_type_mt operator[](long pos) const {
            return store[pos];
        }

        void init() {
            store.init();
        }

        void init(size_mt cap) {
            store.init(cap);
        }

        size_mt rank(size_mt i) const {
            if (i < 0)
                return 0;
            size_mt bid = i / B;
            size_mt off = i % B;
            size_mt fid = bid / F;
            size_mt rk = f_tabs[fid];
            for (size_mt j = fid * F; j < bid; ++j) {
                rk += rrr_table[store.data()[j]][7];
            }
            rk += rrr_table[store.data()[bid]][off];
            return rk;
        }

        void destroy();
    };
}


#endif //RRR_RRR_H
