//
// Created by Sanhu Li on 06/06/17.
//

#ifndef RRR_WAVELETTREE_H
#define RRR_WAVELETTREE_H

#include "RRR.h"

namespace DataStructure {
    /**
     * WaveletTree for AccSeq
     *
     * L for 0, R for 1
     *
     * Root {{A,C,G,T,N,$}} -- trees[0]
     * L {{N,$}} -- trees[1]
     * R {{A,C,G,T}} -- trees[2]
     * RL {{A,C}} -- trees[3] RR {{G,T}} -- trees[4]
     */
    class WaveletTree {
    public:

        RRR trees[5];
        void init() {
            for (auto &tree : trees) {
                tree.init();
            }
        }

        size_mt rank(char c, size_mt pos) const {
            size_mt r;
            pos += 1;
            switch (c) {
                case 'A':
                case 'a':
                    pos = trees[0].rank(pos - 1);
                    pos -= trees[2].rank(pos - 1);
                    pos -= trees[3].rank(pos - 1);
                    return pos;
                case 'C':
                case 'c':
                    pos = trees[0].rank(pos - 1);
                    pos -= trees[2].rank(pos - 1);
                    pos = trees[3].rank(pos - 1);
                    return pos;
                case 'G':
                case 'g':
                    pos = trees[0].rank(pos - 1);
                    pos = trees[2].rank(pos - 1);
                    pos -= trees[4].rank(pos - 1);
                    return pos;
                case 'T':
                case 't':
                    pos = trees[0].rank(pos - 1);
                    pos = trees[2].rank(pos - 1);
                    pos = trees[4].rank(pos - 1);
                    return pos;
                case '\0':
                    pos -= trees[0].rank(pos - 1);
                    pos = trees[1].rank(pos - 1);
                    return pos;
                default:
                    pos -= trees[0].rank(pos - 1);
                    pos -= trees[1].rank(pos - 1);
                    return pos;
            }
        }

        void push(char c) {
            switch (c) {
                case 'A':
                case 'a':
                    trees[0].push(1);
                    trees[2].push(0);
                    trees[3].push(0);
                    break;
                case 'C':
                case 'c':
                    trees[0].push(1);
                    trees[2].push(0);
                    trees[3].push(1);
                    break;
                case 'G':
                case 'g':
                    trees[0].push(1);
                    trees[2].push(1);
                    trees[4].push(0);
                    break;
                case 'T':
                case 't':
                    trees[0].push(1);
                    trees[2].push(1);
                    trees[4].push(1);
                    break;
                case '$':
                case '\0':
                    trees[0].push(0);
                    trees[1].push(1);
                    break;
                default:
                    trees[0].push(0);
                    trees[1].push(0);
                    break;
            }
        }
    };
}

#endif //RRR_WAVELETTREE_H
