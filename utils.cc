//
// Created by Sanhu Li on 10/05/17.
//

#include "utils.h"

std::chrono::time_point<std::chrono::system_clock> begin = std::chrono::system_clock::now();

long file_size(FILE *stream) {
    long pos = ftell(stream);
    fseek(stream, 0, SEEK_END);
    long sz = ftell(stream);
    fseek(stream, pos, SEEK_SET);
    return sz;
}

double get_prog_time(std::chrono::time_point<std::chrono::system_clock> tp) {
    auto now = std::chrono::system_clock::now();
    std::chrono::duration<double> t = now - tp;
    return t.count();
}

void read_lines(const char *path, std::vector<const char *> &lines) {
    auto inf = fopen(path, "r");
    auto sz = static_cast<size_t>(file_size(inf));
    auto content = new char[sz + 1];
    content[sz] = '\0';
    read_file(content, char, sz, path)
    auto cur = content;
    for (size_t i = 0; i < sz + 1; ++i) {
        if (content[i] == '\n') {
            lines.push_back(cur);
            content[i] = '\0';
            cur = content + i + 1;
        }
    }
    lines.push_back(cur);
    fclose(inf);
}
