//
// Created by Sanhu Li on 10/06/17.
//
#include <cstring>
#include <cstdlib>
#include <set>
#include "accSeq.h"
#include "logger.h"


//static void slice(const char *db, int start, size_t length, char *buf);
static void usage(const char *program);

static void single_end(const char *argv[]);
static void pair_end(const char *argv[]);

int main(int argc, const char *argv[]) {

    if (argc < 4 || argc > 5) {
        usage(argv[0]);
        exit(-1);
    }

    if (argc == 4) {
        single_end(argv);
    } else if (argc == 5) {
        pair_end(argv);
    }

    return 0;
}

static void usage(const char *program) {
    Logger::Error(program, "{REFERENCE_FILE}", "{QUERY_FILE}", "{OUTPUT_PATH}");
    Logger::Error(program, "{REFERENCE_FILE}", "{QUERY_FILE_1}", "{QUERY_FILE_2}", "{OUTPUT_PATH}");
    Logger::Error("\t For example: ", program, "reference.fa queries.fq", "for single-end reads.");
    Logger::Error("\t For example: ", program, "reference.fa queries-1.fq queries-2.fq", "for pair-end reads.");
}

//void slice(const char *db, int start, size_t length, char *buf) {
//    strncpy(buf, db + start, length);
//    buf[length] = 0;
//}

static void process_seq_name(char *seq_name) {
    size_t len = strlen(seq_name);
    for (int i = 0; i < len; ++i) {
        if (seq_name[i] == ' ' || seq_name[i] == '\t' || seq_name[i] == '\n' ||
            seq_name[i] == '\r' || seq_name[i] == '/') {
            seq_name[i] = 0;
            break;
        }
    }
}

static void array_cpy(char *dst, const char *src, size_t n) {
    for (size_t i = 0; i < n; ++i) {
        dst[i] = src[i];
    }
}

static void concat_seq(std::vector<Query> &queries, context *global_context) {
    // assume all queries are of same length
    if (queries.empty()) {
        return;
    }

    auto qlen = queries[0].len;
    auto qnum = queries.size();
    auto seq = new char[qlen * qnum];
    for (size_t i = 0; i < queries.size(); ++i) {
        array_cpy(seq + i * qlen, queries[i].seq, qlen);
        free(queries[i].seq);
        queries[i].seq = seq + i * qlen;
    }

    global_context->reads_seq_length = qlen * qnum;
    global_context->reads_seq = seq;
    global_context->read_length = qlen;
}

void single_end(const char **argv) {
    context ctx{};
    ctx.out_path = argv[3];
    std::vector<Query> queries, reads;
    std::vector<Align> alns;

    Logger::Verbose("Done initializing, begin loading reference file", argv[1]);
    auto b = get_prog_time();
    load_index(argv[1], &ctx);
    Logger::Verbose("Done loading reference in", FTOA(get_prog_time() - b), "s");

    Logger::Verbose("Begin loading queries from", argv[2]);
    b = get_prog_time();
    read_queries(queries, argv[2]);
    concat_seq(queries, &ctx);
    ctx.num_queries = queries.size();
    ctx.queries = queries.data();
    Logger::Verbose("Done loading queries in", FTOA(get_prog_time() - b), "s");
    Logger::Verbose(FTOA(queries.size()), "queries loaded.");

    Logger::Verbose("Begin processing queries");
    b = get_prog_time();
    process_queries(queries, reads);
    ctx.reads = reads.data();
    Logger::Verbose("Done processing queries in", FTOA(get_prog_time() - b), "s");
    Logger::Verbose(FTOA(reads.size()), "queries generated.");

    Logger::Verbose("Begin alignment process...");
    b = get_prog_time();
    alignment(alns, reads, &ctx);
    Logger::Verbose("Done alignment in", FTOA(get_prog_time() - b), "s");


    Logger::Verbose("Begin parsing alignment results");
    b = get_prog_time();

    std::vector<Result> results;
    process_align(results, alns, &ctx);
    Logger::Verbose("Done parsing in", FTOA(get_prog_time() - b), "s");

    std::set<std::string> qnames;
    for (auto q : queries) {
        auto qname = strdup(q.name);
        process_seq_name(qname);
        qnames.insert(qname);
    }
//    Logger::Verbose(FTOA(qnames.size()), "total different names");

    std::set<std::string> rnames;
    for (auto r : results) {
        if (r.pos != -1) {
            auto qname = strdup(queries[r.qid].name);
            process_seq_name(qname);
            if (qnames.count(qname) == 0) {
                Logger::Error("None existing query name in results", qname);
            } else {
                rnames.insert(qname);
            }
        }
    }

//    Logger::Verbose(FTOA(rnames.size()), "total results");
    Logger::Verbose("Sensitivity:", FTOA(rnames.size()/float(qnames.size())));

    int count = 0;
    for (auto r : results) {
//        std::cout << r.ed << std::endl;
        /// Assume 5 percent errors and 20 safe offset
        if (r.ed > ctx.read_length * .05 + 20) {
            count++;
        }
    }
    Logger::Verbose("Error rate:", FTOA(count/float(qnames.size())));

    Logger::Verbose("Begin writing results to SAM file");
    b = get_prog_time();
    write_result(results, &ctx);
    Logger::Verbose("Done writing file in", FTOA(get_prog_time() - b), "s");
    Logger::Verbose("Successfully aligned", FTOA(results.size()), "out of", FTOA(queries.size()), "queries.");
}

void pair_end(const char **argv) {
    context ctx{};
    ctx.out_path = argv[4];
    std::vector<Query> queries, reads;
    std::vector<Align> alns, alns2;

    Logger::Verbose("Done initializing, begin loading reference file", argv[1]);
    auto b = get_prog_time();
    load_index(argv[1], &ctx);
    Logger::Verbose("Done loading reference in", FTOA(get_prog_time() - b), "s");

    Logger::Verbose("Begin loading queries from", argv[2], "and", argv[3]);
    b = get_prog_time();
    read_queries(queries, argv[2]);
    read_queries(queries, argv[3]);
    concat_seq(queries, &ctx);
    ctx.num_queries = queries.size();
    auto total_queries = queries.size() / 2;
    ctx.queries = queries.data();
    Logger::Verbose("Done loading queries in", FTOA(get_prog_time() - b), "s");
    Logger::Verbose(FTOA(queries.size()), "queries loaded.");

    Logger::Verbose("Begin processing queries");
    b = get_prog_time();

//    std::cout << queries[0].seq << std::endl;

    process_queries(queries, reads);
    ctx.reads = reads.data();
    Logger::Verbose("Done processing queries in", FTOA(get_prog_time() - b), "s");
    Logger::Verbose(FTOA(reads.size()), "queries generated.");

//    for (int i = 0; i < 31; ++i) {
//        std::cout << std::string(reads[i].off, ' ');
//        std::cout << strndup(reads[i].seq, reads[i].len) << std::endl;
//    }

    Logger::Verbose("Begin alignment process...");
    b = get_prog_time();
    alignment(alns, reads, &ctx);
//    alignment2(alns2, reads, &ctx);

//    size_t cc = 0;
//    std::cout << "Begin checking..." << alns.size() << " " << alns2.size() << std::endl;
//    for (size_t i = 0; i < alns.size(); ++i) {
//        if (alns[i].rid == alns2[i].rid &&
//            alns[i].qid == alns2[i].qid &&
//            alns[i].first == alns2[i].first &&
//            alns[i].second == alns2[i].second &&
//            alns[i].off == alns2[i].off) {
//            cc++;
//        } else {
//            std::cout << "=====================" << std::endl;
//            std::cout << alns[i].rid << " " << alns2[i].rid << std::endl;
//            std::cout << alns[i].qid << " " << alns2[i].qid << std::endl;
//            std::cout << alns[i].first << " " << alns2[i].first << std::endl;
//            std::cout << alns[i].second << " " << alns2[i].second << std::endl;
//            std::cout << alns[i].off << " " << alns2[i].off << std::endl;
//        }
//    }


    Logger::Verbose("Done alignment in", FTOA(get_prog_time() - b), "s");
//    for (auto a : alns) {
//        if (a.qid == 0) {
//            std::cout << a.first << " " << a.second << std::endl;
//            std::cout << strndup(reads[a.rid].seq, reads[a.rid].len) << std::endl;
//        }
//    }


    Logger::Verbose("Begin parsing alignment results");
    b = get_prog_time();

    std::vector<Result> results;
    process_align(results, alns, &ctx);
//    process_align(results, alns2, &ctx);
    Logger::Verbose("Done parsing in", FTOA(get_prog_time() - b), "s");

    std::set<std::string> qnames;
    for (auto q : queries) {
        auto qname = strdup(q.name);
        process_seq_name(qname);
        qnames.insert(qname);
    }
//    Logger::Verbose(FTOA(qnames.size()), "total different names");

    std::set<std::string> rnames;
    for (auto r : results) {
        if (r.pos != -1) {
            auto qname = strdup(queries[r.qid].name);
            process_seq_name(qname);
            if (qnames.count(qname) == 0) {
                Logger::Error("None existing query name in results", qname);
            } else {
                rnames.insert(qname);
            }
        }
    }

//    Logger::Verbose(FTOA(rnames.size()), "total results");
    Logger::Verbose("Sensitivity:", FTOA(rnames.size()/float(total_queries)));

    int count = 0;
//    for (auto r : results) {
//        if (r.ed != -1) {
//            count++;
//        }
//    }

//    std::cout << count << std::endl;

//    count = 0;

    for (auto r : results) {
//        std::cout << r.ed << std::endl;
        /// Assume 5 percent errors and 20 safe offset
//        if (r.pos != -1 and r.ed > ctx.read_length * .05 + 20) {
        if (r.pos != -1 and r.ed > ctx.read_length * .05) {
//            Logger::Error(r.qid);
            count++;
        }
    }
    Logger::Verbose("Error rate:", FTOA(count/float(total_queries)));

    Logger::Verbose("Begin writing results to SAM file");
    b = get_prog_time();
    write_result(results, &ctx);
    Logger::Verbose("Done writing file in", FTOA(get_prog_time() - b), "s");
    Logger::Verbose("Successfully aligned", FTOA(results.size() / 2), "out of", FTOA(total_queries), "reads.");
}
